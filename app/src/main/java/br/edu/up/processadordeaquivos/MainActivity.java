package br.edu.up.processadordeaquivos;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    EditText txtInterno;
    EditText txtExterno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtInterno = (EditText) findViewById(R.id.editText);
        txtExterno = (EditText) findViewById(R.id.editText2);
    }

    public void onClickGravarInterno(View v){
        try {
            FileOutputStream fos = openFileOutput("arquivo.txt", 0);

            String str = txtInterno.getText().toString();
            fos.write(str.getBytes());
            fos.close();
            txtInterno.setText("");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onClickCarregarInterno(View v){
        try {
            FileInputStream fis = openFileInput("arquivo.txt");
            byte[] buffer = new byte[fis.available()];
            fis.read(buffer);
            String str = new String(buffer);
            txtInterno.setText(str);
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onClickExcluirInterno(View v){
        File pasta = getFilesDir();
        File arquivo = new File(pasta, "arquivo.txt");
        if(arquivo.exists()) {
            arquivo.delete();
        }
    }


    public void onClickGravarExterno(View v){
        try {
            File sdCard = Environment.getExternalStorageDirectory();
            File arquivo = new File(sdCard, "arquivo.txt");
            Log.d("teste", "ok.....");
            FileOutputStream fos = new FileOutputStream(arquivo);
            String str = txtExterno.getText().toString();
            fos.write(str.getBytes());
            fos.close();
            txtExterno.setText("");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onClickCarregarExterno(View v){
        try {
            File sdCard = Environment.getExternalStorageDirectory();
            File arquivo = new File(sdCard, "arquivo.txt");
            FileInputStream fis = new FileInputStream(arquivo);
            byte[] buffer = new byte[fis.available()];
            fis.read(buffer);
            String str = new String(buffer);
            txtExterno.setText(str);
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onClickExcluirExterno(View v){
        File pasta = Environment.getExternalStorageDirectory();
        File arquivo = new File(pasta, "arquivo.txt");
        if(arquivo.exists()) {
            arquivo.delete();
        }
    }

}
